﻿using Book.Model;
using Book.View;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Book.Controller
{
    public class BookController
    {
        object pwindow;
        SaveFileDialog sfdialog;
        OpenFileDialog ofdialog;
        public BookController(BookWindow window)
        {
            pwindow = window;
            sfdialog = new SaveFileDialog();
            ofdialog = new OpenFileDialog();
        }
        

        public void BookEventHandler(object sender, RoutedEventArgs e)
        {
            Button B = (Button)sender;
            switch (B.Name)
            {
                case "SaveButton":
                    SaveData();
                    break;
                case "OpenButton":
                    OpenFile();
                    break;
            }
        }

        private void OpenFile()
        {
            ofdialog.Filter = "Xml File (*.Xml)|*.Xml";
            if (ofdialog.ShowDialog() == true)
            {
                Bookm p = new Bookm();
                if (pwindow.GetType().Equals(typeof(BookWindow)))
                {
                    ((BookWindow)pwindow).SetData(p.FromXml(ofdialog.FileName));
                }
               
            }


        }
    

        private void SaveData()
        {
            sfdialog.Filter = "Xml File (*.Xml)|*.Xml";
            if (sfdialog.ShowDialog() == true)
            {
                Bookm p;
                if (pwindow.GetType().Equals(typeof(BookWindow)))
                {
                    p = ((BookWindow)pwindow).GetData();
                    p.ToXml(sfdialog.FileName);
                }
                

                
            }
        }

    }

       
}

