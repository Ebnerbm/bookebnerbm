﻿using Book.Interfaces;
using Book.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Book.Model
{
    [Serializable]
    public class Bookm : IToFile, IFromFile<Bookm>
    {
        public string Año { get; set; }
        public string Name { get; set; }
        public string Autor { get; set; }
        public string State { get; set; }
        public string Editorial { get; set; }

        

        public Bookm FromXml(string filepath)
        {
            return XmlSerialization.ReadFromXmlFile<Bookm>(filepath);
        }

        public void ToXml(string filepath)
        {
            XmlSerialization.WriteToXmlFile(filepath, this);
        }
    }
}
