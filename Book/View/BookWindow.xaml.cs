﻿using Book.Controller;
using Book.Interfaces;
using Book.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Book.View
{
    /// <summary>
    /// Interaction logic for BookWindow.xaml
    /// </summary>
    public partial class BookWindow : Window, IForm<Bookm>
    {
        BookController bc;
        public BookWindow()
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            SetupController();
        }
        public Bookm GetData()
        {
            Bookm book = new Bookm();
            book.Name = NameTextBox.Text;
            book.Autor = AutorTextBox.Text;
            book.Editorial = EditorialTextBox.Text;
            book.Año = AñoTextBox.Text;

            book.State = StateCombobox.SelectedValue.ToString();
            return book;
        }

        public void SetData(Bookm book)
        {
            NameTextBox.Text = book.Name;
            AutorTextBox.Text = book.Autor;
            AñoTextBox.Text = book.Año;
            EditorialTextBox.Text = book.Editorial;
            StateCombobox.SelectedValue = book.State;
        }

        protected void SetupController()
        {
            bc = new BookController(this);
            this.SaveButton.Click += new RoutedEventHandler(bc.BookEventHandler);
            this.OpenButton.Click += new RoutedEventHandler(bc.BookEventHandler);
        }
    }
}
